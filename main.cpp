#include <iostream>
#include <windows.h>


int main()
{
    
    for (int hours = 24; hours >= 0; hours--)
    {
                if (hours < 0)
                {
                    break;
                }
        for (int minutes = 59; minutes >= 0; minutes--)
        {
                if (minutes < 0)
                {
                    minutes = 0;
                    break;
                }
            for (int seconds = 59; seconds >= 0; seconds--)
            {
                if (seconds >= 10)
                {
                    std::cout << hours << ":" << minutes << ":" << seconds;
                    Sleep(1000);
                }
                if (seconds < 10)
                {
                    std::cout << hours << ":" << minutes << ":" << "0" << seconds;
                    Sleep(1000);
                }
                if (seconds == 0)
                {
                    seconds = 0;
                    system("cls");
                    break;
                }
                system("cls");
            }
        }
    }

    std::cout << "Done";
    

    return 0;
}